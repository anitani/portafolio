import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, ScrollView, Image, SafeAreaView } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import nflLogo from './assets/NFL_logo.png'; 

const url = 'https://jsonplaceholder.typicode.com/todos';

const MenuScreen = ({ navigation }) => {
  const [todos, setTodos] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch(url);
        const data = await response.json();
        setTodos(data);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };

    fetchData();
  }, []);

  const displayTodos = (option) => {
    let filteredData;
    switch (option) {
      case '-- Lista de todos los penditentes (solo IDs) --':
        filteredData = todos;
        break;
      case '-- Lista de todos los penditentes (IDs y Titles) --':
        filteredData = todos.map(todo => ({ id: todo.id, title: todo.title }));
        break;
      case '-- Lista de todos los penditentes sin resolver (ID y Title) --':
        filteredData = todos.filter(todo => !todo.completed).map(todo => ({ id: todo.id, title: todo.title }));
        break;
      case '-- Lista de todos los penditentes resueltos (ID y Title) --':
        filteredData = todos.filter(todo => todo.completed).map(todo => ({ id: todo.id, title: todo.title }));
        break;
      case '-- Lista de todos los penditentes (IDs y userID) --':
        filteredData = todos.map(todo => ({ id: todo.id, userId: todo.userId }));
        break;
      case  '-- Lista de todos los penditentes resueltos (ID y userID) --':
        filteredData = todos.filter(todo => !todo.completed).map(todo => ({ id: todo.id, userId: todo.userId }));
        break;
      case '-- Lista de todos los penditentes sin resolver (ID y userID) --':
        filteredData = todos.filter(todo => todo.completed).map(todo => ({ id: todo.id, userId: todo.userId }));
        break;
      default:
        filteredData = [];
    }

    navigation.navigate('TodoDetails', { todos: filteredData });
  };

  return (
    <SafeAreaView style={styles.safeArea}>
      <ScrollView contentContainerStyle={styles.container}>
        <Image source={nflLogo} style={styles.logo} resizeMode="contain" />
        {['-- Lista de todos los penditentes (solo IDs) --', 
        '-- Lista de todos los penditentes (IDs y Titles) --', 
        '-- Lista de todos los penditentes sin resolver (ID y Title) --', 
        '-- Lista de todos los penditentes resueltos (ID y Title) --', 
        '-- Lista de todos los penditentes (IDs y userID) --', 
        '-- Lista de todos los penditentes resueltos (ID y userID) --', 
        '-- Lista de todos los penditentes sin resolver (ID y userID) --']
        .map(option => (
          <TouchableOpacity
            key={option}
            style={styles.optionButton}
            onPress={() => displayTodos(option)}
          >
            <Text style={styles.optionText}>{option}</Text>
          </TouchableOpacity>
        ))}
      </ScrollView>
    </SafeAreaView>
  );
};

// Pantalla de Detalles de los Todos
const TodoDetailsScreen = ({ route }) => {
  const { todos } = route.params;

  return (
    <ScrollView style={styles.resultsContainer}>
      {todos.map((todo, index) => (
        <View key={todo.id.toString()} style={styles.tableRow}>
          <Text style={styles.tableCell}>{`ID: ${todo.id}`}</Text>
          {todo.title && <Text style={styles.tableCell}>{`Title: ${todo.title}`}</Text>}
          {todo.userId && <Text style={styles.tableCell}>{`User: ${todo.userId}`}</Text>}
        </View>
      ))}
    </ScrollView>
  );
};

const Stack = createNativeStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Menu" component={MenuScreen} options={{ title: 'Pendientes NFL' }} />
        <Stack.Screen name="TodoDetails" component={TodoDetailsScreen} options={{ title: 'Detalles de lo elegido' }} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
  },
  container: {
    alignItems: 'center',
    padding: 20,
  },
  logo: {
    width: '100%',
    height: 200, 
    marginBottom: 10,
  },
  optionButton: {
    width: '90%',
    padding: 8,
    backgroundColor: '#ff99cc',
    borderRadius: 8,
    marginVertical: 4,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 10,
  },
  optionText: {
    color: '#333333',
    fontWeight: 'bold',
    textAlign: 'center',
    fontFamily: 'Tenorite',
    fontSize: 20,
  },
  resultsContainer: {
    padding: 2,
  },
  tableRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 2,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
    marginHorizontal: 2,
  },
  tableCell: {
    flex: 0.5,
    fontFamily: 'Tenorite',
    fontSize: 20,
  },
});

export default App;