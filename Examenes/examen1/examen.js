    // menu 
    console.log('-------- Menú --------');
    console.log('1. Mostrar todos los pendientes (solo IDs)');
    console.log('2. Mostrar todos los pendientes (IDs y Titles)');
    console.log('3. Mostrar todos los pendientes SIN RESOLVER (ID y Title)');
    console.log('4. Mostrar todos los pendientes RESUELTOS (ID y Title)');
    console.log('5. Mostrar todos los pendientes (IDs y userID)');
    console.log('6. Mostrar todos los pendientes SIN RESOLVER (ID y userID)');
    console.log('7. Mostrar todos los pendientes RESUELTOS (ID y userID)'); 
    console.log('0. Salir'); 
const read = require('readline');

const rl = read.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.on('close', () => {
    console.log('Bye bye');
    process.exit(0);
});

const url = "https://jsonplaceholder.typicode.com/todos";

rl.setPrompt('Seleccione una opción del 0 - 7: ');
rl.prompt();

rl.on('line', (opcion) => {
    const option = parseInt(opcion);
    if (option >= 0 && option <= 7) {
        fetchMenu(option);
    } else {
        console.log("Opción inválida. Elija una opción de 0 - 7");
        rl.prompt();
    }
});

function fetchMenu(option) {
    fetch(url)
        .then(response => response.json())
        .then(response => {
            switch (option) {
                case 1:
                    console.log('Lista de todos los pendientes (solo IDs):');
                    response.forEach(element => {
                        console.log("- - ID: " + element.id);
                    });
                    break;
                case 2:
                    console.log('Lista de todos los pendientes (IDs y Titles):');
                    response.forEach(element => {
                        console.log("- - ID: " + element.id + " Title: " + element.title);
                    });
                    break;
                case 3:
                    console.log('Lista de todos los pendientes sin resolver (ID y Title):');
                    response
                        .filter(element => !element.completed)
                        .forEach(element => {
                            console.log("- - ID: " + element.id + " Title: " + element.title);
                        });
                    break;
                case 4:
                    console.log('Lista de todos los pendientes resueltos (ID y Title):');
                    response
                        .filter(element => element.completed)
                        .forEach(element => {
                            console.log("- - ID: " + element.id + " Title: " + element.title);
                        });
                    break;
                case 5:
                    console.log('Lista de todos los pendientes (IDs y userID):');
                    response.forEach(element => {
                        console.log("- - ID: " + element.id + " UserId: " + element.userId);
                    });
                    break;
                case 6:
                    console.log('Lista de todos los pendientes sin resolver (ID y userID):');
                    response
                        .filter(element => !element.completed)
                        .forEach(element => {
                            console.log("- - ID: " + element.id + " UserId: " + element.userId);
                        });
                    break;
                case 7:
                    console.log('Lista de todos los pendientes resueltos (ID y userID):');
                    response
                        .filter(element => element.completed)
                        .forEach(element => {
                            console.log("- - ID: " + element.id + " UserId: " + element.userId);
                        });
                    break;
                case 0:
                    rl.close();
                    break;
                default:
                    console.log('Opción no válida. Elija una opción del 0 al 7.');
                    break;
            }
            rl.prompt();
        })
        .catch(error => {
            console.error('Error al obtener los datos:', error);
            rl.prompt();
        });
}
